package pl.typer.repository;

import org.springframework.data.repository.query.Param;
import pl.typer.domain.Payment;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Payment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    @Query("select payment from Payment payment where payment.user.login = ?#{principal.username}")
    List<Payment> findByUserIsCurrentUser();

    @Query("select payment from Payment payment where payment.league.id =:id")
    List<Payment> findByLeague(@Param("id") Long id);

}
