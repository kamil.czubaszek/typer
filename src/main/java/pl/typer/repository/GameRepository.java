package pl.typer.repository;

import org.springframework.data.repository.query.Param;
import pl.typer.domain.Game;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Game entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GameRepository extends JpaRepository<Game, Long> {

    @Query("select game from Game game where game.league.id =:id")
    List<Game> findByLeagueId(@Param("id") Long id);
}
