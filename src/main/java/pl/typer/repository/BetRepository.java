package pl.typer.repository;

import org.springframework.data.repository.query.Param;
import pl.typer.domain.Bet;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Bet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BetRepository extends JpaRepository<Bet, Long> {

    @Query("select bet from Bet bet where bet.user.login = ?#{principal.username}")
    List<Bet> findByUserIsCurrentUser();

    @Query("select bet from Bet bet where bet.game.league.id =:id")
    List<Bet> findByLeagueId(@Param("id") Long id);
}
