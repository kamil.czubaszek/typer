package pl.typer.repository;

import pl.typer.domain.Game;
import pl.typer.domain.League;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the League entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LeagueRepository extends JpaRepository<League, Long> {
    @Query("select distinct league from League league left join fetch league.teams")
    List<League> findAllWithEagerRelationships();

    @Query("select league from League league left join fetch league.teams where league.id =:id")
    League findOneWithEagerRelationships(@Param("id") Long id);
}
