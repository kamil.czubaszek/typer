package pl.typer.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(pl.typer.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(pl.typer.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(pl.typer.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(pl.typer.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(pl.typer.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(pl.typer.domain.User.class.getName() + ".bets", jcacheConfiguration);
            cm.createCache(pl.typer.domain.SocialUserConnection.class.getName(), jcacheConfiguration);
            cm.createCache(pl.typer.domain.Game.class.getName(), jcacheConfiguration);
            cm.createCache(pl.typer.domain.Team.class.getName(), jcacheConfiguration);
            cm.createCache(pl.typer.domain.Game.class.getName() + ".hosts", jcacheConfiguration);
            cm.createCache(pl.typer.domain.Game.class.getName() + ".guests", jcacheConfiguration);
            cm.createCache(pl.typer.domain.Bet.class.getName(), jcacheConfiguration);
            cm.createCache(pl.typer.domain.League.class.getName(), jcacheConfiguration);
            cm.createCache(pl.typer.domain.League.class.getName() + ".teams", jcacheConfiguration);
            cm.createCache(pl.typer.domain.Team.class.getName() + ".leagues", jcacheConfiguration);
            cm.createCache(pl.typer.domain.League.class.getName() + ".games", jcacheConfiguration);
            cm.createCache(pl.typer.domain.Game.class.getName() + ".leagues", jcacheConfiguration);
            cm.createCache(pl.typer.domain.Payment.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
