package pl.typer.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Bet.
 */
@Entity
@Table(name = "bet")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Bet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jhi_date")
    private Instant date;

    @Max(value = 20)
    @Column(name = "host_result")
    private Integer hostResult;

    @Max(value = 20)
    @Column(name = "guest_result")
    private Integer guestResult;

    @Column(name = "jhi_type")
    private Integer type;

    @Column(name = "winner")
    private Integer winner;

    @ManyToOne(optional = false)
    @NotNull
    private Game game;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public Bet date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Integer getHostResult() {
        return hostResult;
    }

    public Bet hostResult(Integer hostResult) {
        this.hostResult = hostResult;
        return this;
    }

    public void setHostResult(Integer hostResult) {
        this.hostResult = hostResult;
    }

    public Integer getGuestResult() {
        return guestResult;
    }

    public Bet guestResult(Integer guestResult) {
        this.guestResult = guestResult;
        return this;
    }

    public void setGuestResult(Integer guestResult) {
        this.guestResult = guestResult;
    }

    public Integer getType() {
        return type;
    }

    public Bet type(Integer type) {
        this.type = type;
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getWinner() {
        return winner;
    }

    public Bet winner(Integer winner) {
        this.winner = winner;
        return this;
    }

    public void setWinner(Integer winner) {
        this.winner = winner;
    }

    public Game getGame() {
        return game;
    }

    public Bet game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public User getUser() {
        return user;
    }

    public Bet user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bet bet = (Bet) o;
        if (bet.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bet.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bet{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", hostResult=" + getHostResult() +
            ", guestResult=" + getGuestResult() +
            ", type=" + getType() +
            ", winner=" + getWinner() +
            "}";
    }
}
