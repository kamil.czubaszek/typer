package pl.typer.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Game.
 */
@Entity
@Table(name = "game")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Game implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "host_result")
    private Integer hostResult;

    @Column(name = "guest_result")
    private Integer guestResult;

    @NotNull
    @Column(name = "jhi_date", nullable = false)
    private Instant date;

    @ManyToOne(optional = false)
    @NotNull
    private Team host;

    @ManyToOne(optional = false)
    @NotNull
    private Team guest;

    @ManyToOne
    private League league;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getHostResult() {
        return hostResult;
    }

    public Game hostResult(Integer hostResult) {
        this.hostResult = hostResult;
        return this;
    }

    public void setHostResult(Integer hostResult) {
        this.hostResult = hostResult;
    }

    public Integer getGuestResult() {
        return guestResult;
    }

    public Game guestResult(Integer guestResult) {
        this.guestResult = guestResult;
        return this;
    }

    public void setGuestResult(Integer guestResult) {
        this.guestResult = guestResult;
    }

    public Instant getDate() {
        return date;
    }

    public Game date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Team getHost() {
        return host;
    }

    public Game host(Team team) {
        this.host = team;
        return this;
    }

    public void setHost(Team team) {
        this.host = team;
    }

    public Team getGuest() {
        return guest;
    }

    public Game guest(Team team) {
        this.guest = team;
        return this;
    }

    public void setGuest(Team team) {
        this.guest = team;
    }

    public League getLeague() {
        return league;
    }

    public Game league(League league) {
        this.league = league;
        return this;
    }

    public void setLeague(League league) {
        this.league = league;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Game game = (Game) o;
        if (game.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), game.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Game{" +
            "id=" + getId() +
            ", hostResult=" + getHostResult() +
            ", guestResult=" + getGuestResult() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
