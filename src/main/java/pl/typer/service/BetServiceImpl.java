package pl.typer.service;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.typer.domain.Authority;
import pl.typer.domain.Bet;
import pl.typer.domain.User;
import pl.typer.repository.BetRepository;
import pl.typer.service.dto.UserDTO;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class BetServiceImpl implements BetService {
    @Autowired
    private BetRepository betRepository;
    @Autowired
    private UserService userService;

    @Override
    public Bet saveBet(Bet bet) {
        User user = userService.getPrincipal();
        bet.setDate(Instant.now());
        bet.setUser(user);

        if(!equalBets(bet,user) && equalDates(bet.getDate(),bet.getGame().getDate()))
            return betRepository.save(bet);
        return null;
    }

    @Override
    public Bet updateBet(Bet bet) {
        bet.setDate(Instant.now());
        if((userService.isAdmin() || bet.getUser().getId().equals(userService.getPrincipal().getId())) && equalDates(bet.getDate(),bet.getGame().getDate()))
            return betRepository.save(bet);
        return null;
    }

    @Override
    public List<Bet> getBetsByLeague(Long id) {
        return betRepository.findByLeagueId(id);
    }

    private boolean equalBets(Bet bet, User user){
        Set<Bet> bets = user.getBets();
        for (Bet b : bets){
            if(b.getUser().getId().equals(bet.getUser().getId()) && b.getGame().getId().equals(bet.getGame().getId()))
                return true;
        }
        return false;
    }

    private boolean equalDates(Instant betDate, Instant gameDate){
        return betDate.plusSeconds(300).isBefore(gameDate);
    }
}
