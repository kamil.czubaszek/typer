package pl.typer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.typer.domain.Bet;
import pl.typer.domain.League;
import pl.typer.domain.Payment;
import pl.typer.domain.User;
import pl.typer.repository.BetRepository;
import pl.typer.repository.LeagueRepository;
import pl.typer.repository.PaymentRepository;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class LeagueServiceImpl implements LeagueService {
    @Autowired
    private UserService userService;
    @Autowired
    private PaymentRepository paymentRepository;
    @Autowired
    private LeagueRepository leagueRepository;
    @Autowired
    private BetRepository betRepository;

    @Override
    public boolean userAccessToLeague(League league) {
        if(userService.isAdmin())
            return true;
        List<Payment> pays = paymentRepository.findByUserIsCurrentUser();
        User user = userService.getPrincipal();

        for (Payment p : pays){
            if(p.getLeague().getId() == league.getId() && p.getUser().getId() == user.getId())
                return true;
        }
        return false;
    }

    @Override
    public Double getAward(Long id) {
        List<Payment> payments = paymentRepository.findByLeague(id);
        Double award = payments.stream().mapToDouble(Payment::getAmount).sum();
        return award;
    }

    @Override
    public Integer getPointsByUser(Long id) {
        List<Bet> bets = betRepository.findByUserIsCurrentUser().stream().filter(bet -> bet.getGame().getLeague().getId() == id).collect(Collectors.toList());
        Integer points = 0;
        for (Bet bet : bets){
            if(bet.getGame().getHostResult() != null && bet.getGame().getGuestResult() != null){
                if(bet.getHostResult() == bet.getGame().getHostResult() && bet.getGuestResult() == bet.getGame().getGuestResult())
                    points++;
            }
        }
        return points;
    }

    @Override
    public Map<String, Integer> getRank(Long id) {
        Map<String,Integer> rank = new HashMap<>();
        List<Payment> payments = paymentRepository.findByLeague(id);
        User user;
        for (Payment p : payments){
            user = p.getUser();
            rank.put(user.getFirstName() + " " + user.getLastName(),getPointsByUser(id));
        }
        Map<String,Integer> result = rank.entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        return result;
    }
}
