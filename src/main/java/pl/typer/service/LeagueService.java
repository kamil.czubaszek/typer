package pl.typer.service;

import org.springframework.stereotype.Service;
import pl.typer.domain.Bet;
import pl.typer.domain.League;

import java.util.List;
import java.util.Map;

@Service
public interface LeagueService {
    boolean userAccessToLeague(League league);
    Double getAward(Long id);
    Integer getPointsByUser(Long id);
    Map<String,Integer> getRank(Long id);
}
