package pl.typer.service;

import org.springframework.stereotype.Service;
import pl.typer.domain.Bet;

import java.util.List;

@Service
public interface BetService {
    Bet saveBet(Bet bet);
    Bet updateBet(Bet bet);
    List<Bet> getBetsByLeague(Long id);
}
