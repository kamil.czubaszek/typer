/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.typer.web.rest.vm;
