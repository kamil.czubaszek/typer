package pl.typer.web.rest.errors;

public class BetEditForbidden extends BadRequestAlertException {
    public BetEditForbidden() {
        super(ErrorConstants.BET_EDIT_FORBIDDEN, "Bet edit forbidden", "bet-edit-forbidden","bet-edit-forbidden");
    }
}
