package pl.typer.web.rest.errors;

public class BetAlreadyExist extends BadRequestAlertException {
    public BetAlreadyExist() {
        super(ErrorConstants.BET_ALREADY_EXIST, "Bet already exist", "bet-exist","bet-exist");
    }
}
