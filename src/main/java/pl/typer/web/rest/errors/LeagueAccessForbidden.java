package pl.typer.web.rest.errors;

public class LeagueAccessForbidden extends BadRequestAlertException {
    public LeagueAccessForbidden() {
        super(ErrorConstants.LEAGUE_ACCESS_FORBIDDEN, "League access forbidden", "league-access-forbidden","league-access-forbidden");
    }
}
