package pl.typer.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import pl.typer.domain.Bet;

import pl.typer.repository.BetRepository;
import pl.typer.service.BetService;
import pl.typer.service.UserService;
import pl.typer.web.rest.errors.BadRequestAlertException;
import pl.typer.web.rest.errors.BetAlreadyExist;
import pl.typer.web.rest.errors.BetEditForbidden;
import pl.typer.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Bet.
 */
@RestController
@RequestMapping("/api")
public class BetResource {

    private final Logger log = LoggerFactory.getLogger(BetResource.class);

    private static final String ENTITY_NAME = "bet";

    private final BetRepository betRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private BetService betService;

    public BetResource(BetRepository betRepository) {
        this.betRepository = betRepository;
    }

    /**
     * POST  /bets : Create a new bet.
     *
     * @param bet the bet to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bet, or with status 400 (Bad Request) if the bet has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/bets")
    @Timed
    public ResponseEntity<Bet> createBet(@Valid @RequestBody Bet bet) throws URISyntaxException {
        log.debug("REST request to save Bet : {}", bet);
        if (bet.getId() != null) {
            throw new BadRequestAlertException("A new bet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Bet result = betService.saveBet(bet);

        if(result == null)
            throw new BetAlreadyExist();
        return ResponseEntity.created(new URI("/api/bets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /bets : Updates an existing bet.
     *
     * @param bet the bet to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bet,
     * or with status 400 (Bad Request) if the bet is not valid,
     * or with status 500 (Internal Server Error) if the bet couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/bets")
    @Timed
    public ResponseEntity<Bet> updateBet(@Valid @RequestBody Bet bet) throws URISyntaxException {
        log.debug("REST request to update Bet : {}", bet);
        if (bet.getId() == null) {
            return createBet(bet);
        }

        Bet result = betService.updateBet(bet);
        if(result == null)
            throw new BetEditForbidden();
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bet.getId().toString()))
            .body(result);
    }

    /**
     * GET  /bets : get all the bets.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of bets in body
     */
    @GetMapping("/bets")
    @Timed
    public List<Bet> getAllBets() {
        log.debug("REST request to get all Bets");
        return betRepository.findAll();
        }

    @GetMapping("/bets/my-bets")
    @Timed
    public List<Bet> getMyBets() {
        log.debug("REST request to get all my Bets");
        return betRepository.findByUserIsCurrentUser();
    }

    /**
     * GET  /bets/:id : get the "id" bet.
     *
     * @param id the id of the bet to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bet, or with status 404 (Not Found)
     */
    @GetMapping("/bets/{id}")
    @Timed
    public ResponseEntity<Bet> getBet(@PathVariable Long id) {
        log.debug("REST request to get Bet : {}", id);
        Bet bet = betRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(bet));
    }

    /**
     * DELETE  /bets/:id : delete the "id" bet.
     *
     * @param id the id of the bet to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/bets/{id}")
    @Timed
    public ResponseEntity<Void> deleteBet(@PathVariable Long id) {
        log.debug("REST request to delete Bet : {}", id);
        betRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/bets/league/{id}")
    @Timed
    public List<Bet> getBetsByLeague(@PathVariable Long id) {
        log.debug("REST request to get all bets in League");
        return betService.getBetsByLeague(id);
    }
}
