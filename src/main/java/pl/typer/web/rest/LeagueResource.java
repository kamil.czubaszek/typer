package pl.typer.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import pl.typer.domain.Bet;
import pl.typer.domain.Game;
import pl.typer.domain.League;

import pl.typer.repository.GameRepository;
import pl.typer.repository.LeagueRepository;
import pl.typer.service.LeagueService;
import pl.typer.web.rest.errors.BadRequestAlertException;
import pl.typer.web.rest.errors.LeagueAccessForbidden;
import pl.typer.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.*;

/**
 * REST controller for managing League.
 */
@RestController
@RequestMapping("/api")
public class LeagueResource {

    private final Logger log = LoggerFactory.getLogger(LeagueResource.class);

    private static final String ENTITY_NAME = "league";

    private final LeagueRepository leagueRepository;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private LeagueService leagueService;

    public LeagueResource(LeagueRepository leagueRepository) {
        this.leagueRepository = leagueRepository;
    }

    /**
     * POST  /leagues : Create a new league.
     *
     * @param league the league to create
     * @return the ResponseEntity with status 201 (Created) and with body the new league, or with status 400 (Bad Request) if the league has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/leagues")
    @Timed
    public ResponseEntity<League> createLeague(@Valid @RequestBody League league) throws URISyntaxException {
        log.debug("REST request to save League : {}", league);
        if (league.getId() != null) {
            throw new BadRequestAlertException("A new league cannot already have an ID", ENTITY_NAME, "idexists");
        }
        League result = leagueRepository.save(league);
        return ResponseEntity.created(new URI("/api/leagues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /leagues : Updates an existing league.
     *
     * @param league the league to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated league,
     * or with status 400 (Bad Request) if the league is not valid,
     * or with status 500 (Internal Server Error) if the league couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/leagues")
    @Timed
    public ResponseEntity<League> updateLeague(@Valid @RequestBody League league) throws URISyntaxException {
        log.debug("REST request to update League : {}", league);
        if (league.getId() == null) {
            return createLeague(league);
        }
        League result = leagueRepository.save(league);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, league.getId().toString()))
            .body(result);
    }

    /**
     * GET  /leagues : get all the leagues.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of leagues in body
     */
    @GetMapping("/leagues")
    @Timed
    public List<League> getAllLeagues() {
        log.debug("REST request to get all Leagues");
        return leagueRepository.findAllWithEagerRelationships();
        }

    @GetMapping("/leagues/games/{id}")
    @Timed
    public List<Game> getGamesInLeague(@PathVariable Long id) {
        log.debug("REST request to get all games in League");
        return gameRepository.findByLeagueId(id);
    }

    @GetMapping("/leagues/award/{id}")
    @Timed
    public ResponseEntity<Double> getAward(@PathVariable Long id) {
        log.debug("REST request to get award in League");
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(leagueService.getAward(id)));
    }

    @GetMapping("/leagues/rank/{id}")
    @Timed
    public ResponseEntity<Map<String,Integer>> getRank(@PathVariable Long id) {
        log.debug("REST request to get rank in League");
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(leagueService.getRank(id)));
    }

    /**
     * GET  /leagues/:id : get the "id" league.
     *
     * @param id the id of the league to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the league, or with status 404 (Not Found)
     */
    @GetMapping("/leagues/{id}")
    @Timed
    public ResponseEntity<League> getLeague(@PathVariable Long id) {
        log.debug("REST request to get League : {}", id);
        League league = leagueRepository.findOneWithEagerRelationships(id);
        if(!leagueService.userAccessToLeague(league))
            throw new LeagueAccessForbidden();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(league));
    }

    /**
     * DELETE  /leagues/:id : delete the "id" league.
     *
     * @param id the id of the league to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/leagues/{id}")
    @Timed
    public ResponseEntity<Void> deleteLeague(@PathVariable Long id) {
        log.debug("REST request to delete League : {}", id);
        leagueRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/leagues/points/{id}")
    @Timed
    public ResponseEntity<Integer> getUserPoints(@PathVariable Long id) {
        Integer points = leagueService.getPointsByUser(id);
        log.debug("REST request to get user points for League " + points);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(points));
    }
}
