import { Pipe, PipeTransform } from '@angular/core';
import {Game} from '../entities/game';

@Pipe({ name: 'searchByTeamName' })
export class SearchByTeamName implements PipeTransform {
    transform(games: Game[], searchText: string) {
        searchText = searchText.toLowerCase();
        return games.filter((game) => game.splitedteams.toLowerCase().indexOf(searchText) !== -1);
    }
}
