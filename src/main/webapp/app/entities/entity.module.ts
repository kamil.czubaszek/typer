import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TyperGameModule } from './game/game.module';
import { TyperTeamModule } from './team/team.module';
import { TyperBetModule } from './bet/bet.module';
import { TyperLeagueModule } from './league/league.module';
import { TyperPaymentModule } from './payment/payment.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TyperGameModule,
        TyperTeamModule,
        TyperBetModule,
        TyperLeagueModule,
        TyperPaymentModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TyperEntityModule {}
