import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Bet } from './bet.model';
import { BetService } from './bet.service';
import {Game, GameService} from '../game';

@Injectable()
export class BetPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private betService: BetService,
        private gameService: GameService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, gameId?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id > 0) {
                this.betService.find(id)
                    .subscribe((betResponse: HttpResponse<Bet>) => {
                        const bet: Bet = betResponse.body;
                        bet.date = this.datePipe
                            .transform(bet.date, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.betModalRef(component, bet);
                        resolve(this.ngbModalRef);
                    });
            } else if (id < 0) {
                    const bet: Bet = new Bet();
                    this.gameService.find(gameId)
                        .subscribe((gameResponse: HttpResponse<Game>) => {
                            const game: Game = gameResponse.body;
                            /*game.date = this.datePipe
                                .transform(game.date, 'yyyy-MM-ddTHH:mm:ss');*/
                            bet.game = game;
                            this.ngbModalRef = this.betModalRef(component, bet);
                            resolve(this.ngbModalRef);
                        });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.betModalRef(component, new Bet());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    betModalRef(component: Component, bet: Bet): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bet = bet;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
