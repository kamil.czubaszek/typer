import { BaseEntity, User } from './../../shared';
import {Game} from '../game';

export class Bet implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public hostResult?: number,
        public guestResult?: number,
        public type?: number,
        public winner?: number,
        public game?: Game,
        public user?: User
    ) {}
}
