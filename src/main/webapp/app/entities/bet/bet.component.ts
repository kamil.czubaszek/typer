import {Component, OnInit, OnDestroy, Output, EventEmitter} from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Bet } from './bet.model';
import { BetService } from './bet.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-bet',
    templateUrl: './bet.component.html'
})
export class BetComponent implements OnInit, OnDestroy {
    bets: Bet[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private betService: BetService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.betService.myBets().subscribe(
            (res: HttpResponse<Bet[]>) => {
                this.bets = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInBets();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Bet) {
        return item.id;
    }
    registerChangeInBets() {
        this.eventSubscriber = this.eventManager.subscribe('betListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    public compareDates(gameDate): boolean {
        const currentDate = new Date();
        const newGameDate = new Date(gameDate);
        let state = false;
        if (currentDate < newGameDate) {
            state = true;
        }
        return state;
    }
}
