import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Bet } from './bet.model';
import { BetPopupService } from './bet-popup.service';
import { BetService } from './bet.service';
import { Game, GameService } from '../game';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-bet-dialog',
    templateUrl: './bet-dialog.component.html',
    styleUrls: [
        'bet.component.css'
    ]
})
export class BetDialogComponent implements OnInit {

    bet: Bet;
    isSaving: boolean;
    games: Game[];
    users: User[];
    types: any[];
    results: any[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private betService: BetService,
        private gameService: GameService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: HttpResponse<Game[]>) => { this.games = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.loadData();
    }

    loadData() {
        this.types = [
            {
                id: 1,
                name: 'Dokładny wynik'
            },
            {
                id: 2,
                name: 'Zakład prosty'
            },
            {
                id: 3,
                name: 'Zakład mieszany'
            }
        ];

        this.results = [
            {
                id: 1,
                res: this.bet.game.host.name
            },
            {
                id: 0,
                res: 'Remis'
            },
            {
                id: 2,
                res: this.bet.game.guest.name
            }
        ];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bet.id !== undefined) {
            this.subscribeToSaveResponse(
                this.betService.update(this.bet));
        } else {
            this.subscribeToSaveResponse(
                this.betService.create(this.bet));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Bet>>) {
        result.subscribe((res: HttpResponse<Bet>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Bet) {
        this.eventManager.broadcast({ name: 'betListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    changeItemResult(item: number) {
        this.bet.winner = item;
    }
}

@Component({
    selector: 'jhi-bet-popup',
    template: ''
})
export class BetPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private betPopupService: BetPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] > 0 ) {
                this.betPopupService
                    .open(BetDialogComponent as Component, params['id']);
            } else if (params['id'] < 0) {
                this.betPopupService
                    .open(BetDialogComponent as Component, params['id'], params['gameId']);
            } else {
                this.betPopupService
                    .open(BetDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
