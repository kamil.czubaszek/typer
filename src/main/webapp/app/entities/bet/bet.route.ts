import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { BetComponent } from './bet.component';
import { BetDetailComponent } from './bet-detail.component';
import { BetPopupComponent } from './bet-dialog.component';
import { BetDeletePopupComponent } from './bet-delete-dialog.component';

export const betRoute: Routes = [
    {
        path: 'bet',
        component: BetComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'typerApp.bet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'bet/:id',
        component: BetDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'typerApp.bet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const betPopupRoute: Routes = [
    {
        path: 'bet-new',
        component: BetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'typerApp.bet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bet/:id/edit',
        component: BetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'typerApp.bet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bet/:id/:gameId/create',
        component: BetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'typerApp.bet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bet/:id/delete',
        component: BetDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'typerApp.bet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
