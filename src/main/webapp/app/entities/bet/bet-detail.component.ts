import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Bet } from './bet.model';
import { BetService } from './bet.service';
import {BetComponent} from './bet.component';

@Component({
    selector: 'jhi-bet-detail',
    templateUrl: './bet-detail.component.html'
})
export class BetDetailComponent implements OnInit, OnDestroy {

    bet: Bet;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private betService: BetService,
        private route: ActivatedRoute,
        private betComponent: BetComponent
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBets();
    }

    load(id) {
        this.betService.find(id)
            .subscribe((betResponse: HttpResponse<Bet>) => {
                this.bet = betResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBets() {
        this.eventSubscriber = this.eventManager.subscribe(
            'betListModification',
            (response) => this.load(this.bet.id)
        );
    }

    getState() {
        return this.betComponent.compareDates(this.bet.game.date);
    }

    checkSimpleBet() {
        if (this.bet.hostResult === this.bet.guestResult && this.bet.winner === 0) {
            return 1;
        } else if (this.bet.hostResult > this.bet.guestResult && this.bet.winner === 1) {
            return 1;
        } else if (this.bet.guestResult > this.bet.hostResult && this.bet.winner === 2) {
            return 1;
        } else {
            return 0;
        }
    }

    checkCorrectScore() {
        if (this.bet.hostResult === this.bet.game.hostResult && this.bet.guestResult === this.bet.game.guestResult) {
            return 3;
        } else {
            return 0;
        }
    }

    checkMix() {
        const simple = this.checkSimpleBet();
        const correct = this.checkCorrectScore();

        if (simple > 0 && correct > 0) {
            return 5;
        } else {
            return -1;
        }
    }

    getPoints(): number {
        if (this.getState()) {
            return 0;
        } else {
            if (this.bet.type === 1) {
                return this.checkCorrectScore();
            } else if (this.bet.type === 2) {
                return this.checkSimpleBet();
            } else {
                return this.checkMix();
            }
        }
    }
}
