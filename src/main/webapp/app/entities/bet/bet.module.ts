import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TyperSharedModule } from '../../shared';
import { TyperAdminModule } from '../../admin/admin.module';
import {
    BetService,
    BetPopupService,
    BetComponent,
    BetDetailComponent,
    BetDialogComponent,
    BetPopupComponent,
    BetDeletePopupComponent,
    BetDeleteDialogComponent,
    betRoute,
    betPopupRoute,
} from './';

const ENTITY_STATES = [
    ...betRoute,
    ...betPopupRoute,
];

@NgModule({
    imports: [
        TyperSharedModule,
        TyperAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BetComponent,
        BetDetailComponent,
        BetDialogComponent,
        BetDeleteDialogComponent,
        BetPopupComponent,
        BetDeletePopupComponent,
    ],
    entryComponents: [
        BetComponent,
        BetDialogComponent,
        BetPopupComponent,
        BetDeleteDialogComponent,
        BetDeletePopupComponent,
    ],
    providers: [
        BetService,
        BetPopupService,
        BetComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TyperBetModule {}
