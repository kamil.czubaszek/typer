import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TyperSharedModule } from '../../shared';
import {
    LeagueService,
    LeaguePopupService,
    LeagueComponent,
    LeagueDetailComponent,
    LeagueDialogComponent,
    LeaguePopupComponent,
    LeagueDeletePopupComponent,
    LeagueDeleteDialogComponent,
    leagueRoute,
    leaguePopupRoute,
    LeagueBetsComponent
} from './';
import {SearchByTeamName} from '../../pipes/SearchByTeamName';

const ENTITY_STATES = [
    ...leagueRoute,
    ...leaguePopupRoute,
];

@NgModule({
    imports: [
        TyperSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        LeagueComponent,
        LeagueDetailComponent,
        LeagueDialogComponent,
        LeagueDeleteDialogComponent,
        LeaguePopupComponent,
        LeagueDeletePopupComponent,
        LeagueBetsComponent,
        SearchByTeamName
    ],
    entryComponents: [
        LeagueComponent,
        LeagueDialogComponent,
        LeaguePopupComponent,
        LeagueDeleteDialogComponent,
        LeagueDeletePopupComponent,
    ],
    providers: [
        LeagueService,
        LeaguePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TyperLeagueModule {}
