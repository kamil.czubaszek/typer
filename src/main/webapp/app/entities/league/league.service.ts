import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { League } from './league.model';
import { createRequestOption } from '../../shared';
import {Game} from '../game';

export type EntityResponseType = HttpResponse<League>;
export type NumberResponseType = HttpResponse<number>;
export type MapResponseType = HttpResponse<Map<string, number>>;

@Injectable()
export class LeagueService {

    private resourceUrl =  SERVER_API_URL + 'api/leagues';

    constructor(private http: HttpClient) { }

    create(league: League): Observable<EntityResponseType> {
        const copy = this.convert(league);
        return this.http.post<League>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(league: League): Observable<EntityResponseType> {
        const copy = this.convert(league);
        return this.http.put<League>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<League>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    getAward(id: number): Observable<NumberResponseType> {
        return this.http.get<number>(`${this.resourceUrl}/award/${id}`, { observe: 'response'})
            .map((res: NumberResponseType) => this.convertResponseNumber(res));
    }

    getPoints(id: number): Observable<NumberResponseType> {
        return this.http.get<number>(`${this.resourceUrl}/points/${id}`, { observe: 'response'})
            .map((res: NumberResponseType) => this.convertResponseNumber(res));
    }

    getRank(id: number): Observable<MapResponseType> {
        return this.http.get<Map<string, number>>(`${this.resourceUrl}/rank/${id}`, { observe: 'response'})
            .map((res: MapResponseType) => this.convertResponseMap(res));
    }

    findGames(id: number): Observable<HttpResponse<Game[]>> {
        return this.http.get<Game[]>(`${this.resourceUrl}/games/${id}`, { observe: 'response'})
            .map((res: HttpResponse<Game[]>) => this.convertArrayResponse(res));
    }

    query(req?: any): Observable<HttpResponse<League[]>> {
        const options = createRequestOption(req);
        return this.http.get<League[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<League[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: League = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertResponseNumber(res: NumberResponseType): NumberResponseType {
        const body: number = res.body;
        return res.clone({body});
    }

    private convertResponseMap(res: MapResponseType): MapResponseType {
        const body: Map<string, number> = res.body;
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<League[]>): HttpResponse<League[]> {
        const jsonResponse: League[] = res.body;
        const body: League[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to League.
     */
    private convertItemFromServer(league: League): League {
        const copy: League = Object.assign({}, league);
        return copy;
    }

    /**
     * Convert a League to a JSON which can be sent to the server.
     */
    private convert(league: League): League {
        const copy: League = Object.assign({}, league);
        return copy;
    }
}
