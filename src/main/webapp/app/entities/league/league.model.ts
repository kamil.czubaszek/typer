import { BaseEntity } from './../../shared';

export class League implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public teams?: BaseEntity[],
        public games?: BaseEntity[],
        public award?: number,
        public userPoints?: number
    ) {
    }
}
