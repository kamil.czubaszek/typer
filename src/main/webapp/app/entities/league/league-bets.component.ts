import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';
import {Pager} from '../../utils/Pager';
import {Bet, BetService} from '../bet';

@Component({
    selector: 'jhi-league-bets',
    templateUrl: './league-bets.component.html'
})
export class LeagueBetsComponent implements OnInit {

    bets: Bet[];
    private subscription: Subscription;
    pager: any = {};
    pagedItems: any[];
    pageSize = 10;

    constructor(
        private eventManager: JhiEventManager,
        private betService: BetService,
        private route: ActivatedRoute,
        private pagerService: Pager
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
    }

    load(id) {
        this.betService.findBets(id)
            .subscribe((betResponse: HttpResponse<Bet[]>) => {
                this.bets = betResponse.body;
                this.setPage(1);
            });
    }

    previousState() {
        window.history.back();
    }

    setPage(page: number) {
        this.pager = this.pagerService.getPager(this.bets.length, page, this.pageSize);
        this.pagedItems = this.bets.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}
