import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { League } from './league.model';
import { LeagueService } from './league.service';
import {Game} from '../game';
import {Pager} from '../../utils/Pager';

@Component({
    selector: 'jhi-league-detail',
    templateUrl: './league-detail.component.html',
    styleUrls: ['./league.component.css']
})
export class LeagueDetailComponent implements OnInit, OnDestroy {

    league: League;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    games: Game[];
    showAlertAccess = false;
    showTeams = false;
    pager: any = {};
    pagedItems: any[];
    searchTeamName = '';
    pageSize = 10;
    dateFrom = '';
    dateTo = '';

    sizes = [
        {
            size: 10
        },
        {
            size: 20
        },
        {
            size: 50
        },
        {
            size: 100
        }
    ];

    constructor(
        private eventManager: JhiEventManager,
        private leagueService: LeagueService,
        private route: ActivatedRoute,
        private pagerService: Pager
    ) {}

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInLeagues();
    }

    load(id) {
        this.leagueService.find(id)
            .subscribe((leagueResponse: HttpResponse<League>) => {
                this.league = leagueResponse.body;
                this.getGames(id);
            });
        if (this.league === 'undefined') {
            this.showAlertAccess = true;
        }
    }

    getGames(id) {
        this.leagueService.findGames(id)
            .subscribe((leagueResponse: HttpResponse<Game[]>) => {
                this.games = leagueResponse.body;
                this.setSplitedNameTeams();
                this.setPage(1);
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLeagues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'leagueListModification',
            (response) => this.load(this.league.id)
        );
    }

    toggleShowTeams() {
        this.showTeams = !this.showTeams;
    }

    setSplitedNameTeams() {
        for (const game of this.games){
            game.splitedteams = game.host.name + ' vs ' + game.guest.name;
        }
    }

    setPage(page: number, games = this.games) {
        this.pager = this.pagerService.getPager(games.length, page, this.pageSize);
        this.pagedItems = games.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    getFilteredGames(date, kindOfDate): Game[] {
        let filteredGames =  this.games;
        const someDate = new Date(date);
        someDate.setHours(0, 0, 0, 0);
        console.log('Date from form: ' + someDate);

        if (kindOfDate) {
            filteredGames = filteredGames.filter((game) => {
                const gameDate = new Date(game.date);
                gameDate.setHours(0, 0, 0, 0);
                console.log('Date from db: ' + gameDate);
                return gameDate >= someDate;
            });
            if (this.dateTo !== '') {
                const dateTo = new Date(this.dateTo);
                dateTo.setHours(0, 0, 0, 0);
                filteredGames = filteredGames.filter((game) => {
                    const gameDate = new Date(game.date);
                    gameDate.setHours(0, 0, 0, 0);
                    return gameDate <= dateTo;
                });
            }
            return filteredGames;
        } else {
            filteredGames = filteredGames.filter((game) => {
                const gameDate = new Date(game.date);
                gameDate.setHours(0, 0, 0, 0);
                return gameDate <= someDate;
            });
            if (this.dateFrom !== '') {
                const dateFrom = new Date(this.dateFrom);
                dateFrom.setHours(0, 0, 0, 0);
                filteredGames = filteredGames.filter((game) => {
                    const gameDate = new Date(game.date);
                    gameDate.setHours(0, 0, 0, 0);
                    return gameDate >= dateFrom;
                });
            }
            return filteredGames;
        }
    }

    setDateFrom() {
        let filteredGames: Array<Game>;
        if (this.dateFrom !== '') {
            filteredGames = this.getFilteredGames(this.dateFrom, true);
            this.setPage(1, filteredGames);
        } else {
            this.setPage(1);
        }
    }

    setDateTo() {
        let filteredGames: Array<Game>;
        if (this.dateTo !== '') {
            filteredGames = this.getFilteredGames(this.dateTo, false);
            this.setPage(1, filteredGames);
        } else {
            this.setPage(1);
        }
    }
}
