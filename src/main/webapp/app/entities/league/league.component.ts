import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { League } from './league.model';
import { LeagueService } from './league.service';
import { Principal } from '../../shared';
import {Bet} from '../bet';

@Component({
    selector: 'jhi-league',
    templateUrl: './league.component.html'
})
export class LeagueComponent implements OnInit, OnDestroy {
    leagues: League[];
    currentAccount: any;
    eventSubscriber: Subscription;
    showButton: boolean;
    award: number;
    filtersLoaded: Promise<boolean>;
    pointsLoaded: Promise<boolean>;

    constructor(
        private leagueService: LeagueService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.leagueService.query().subscribe(
            (res: HttpResponse<League[]>) => {
                this.additonalInit(res.body);
                this.leagues = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    additonalInit(l) {
        for (const league of l){
            this.loadAward(league);
            this.loadPoints(league);
        }
    }

    loadAward(league) {
        this.leagueService.getAward(league.id)
            .subscribe((leagueResponse: HttpResponse<number>) => {
                league.award = leagueResponse.body;
                this.filtersLoaded = Promise.resolve(true);
            });
    }

    loadPoints(league) {
        this.leagueService.getPoints(league.id)
            .subscribe((leagueResponse: HttpResponse<number>) => {
                league.points = leagueResponse.body;
                this.pointsLoaded = Promise.resolve(true);
            });
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInLeagues();

        if (this.principal.hasAnyAuthorityDirect(['ROLE_ADMIN'])) {
            this.showButton = true;
        } else {
            this.showButton = false;
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: League) {
        return item.id;
    }
    registerChangeInLeagues() {
        this.eventSubscriber = this.eventManager.subscribe('leagueListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
