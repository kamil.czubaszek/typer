import { BaseEntity } from './../../shared';
import {League} from '../league';
import {Team} from '../team';

export class Game implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public hostResult?: number,
        public guestResult?: number,
        public host?: Team,
        public guest?: Team,
        public league?: League,
        public splitedteams?: string
    ) {
    }
}
