import { BaseEntity, User } from './../../shared';

export class Payment implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public amount?: number,
        public league?: BaseEntity,
        public user?: User,
    ) {
    }
}
