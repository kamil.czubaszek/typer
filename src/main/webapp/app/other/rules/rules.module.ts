import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import {TyperSharedModule} from '../../shared';
import {RulesComponent} from './rules.component';
import {RULES_ROUTE} from './rules.route';

@NgModule({
    imports: [
        TyperSharedModule,
        RouterModule.forChild([ RULES_ROUTE ])
    ],
    declarations: [
        RulesComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TyperRulesModule {}
