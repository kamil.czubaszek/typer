import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-rules',
    templateUrl: './rules.component.html',
    styleUrls: [
        'rules.css'
    ]

})
export class RulesComponent implements OnInit {

    bet1 = false;
    bet2 = false;
    bet3 = false;

    constructor() {
    }

    ngOnInit() {

    }

    toggleBet1() {
        this.bet1 = !this.bet1;
        this.bet2 = false;
        this.bet3 = false;
    }

    toggleBet2() {
        this.bet2 = !this.bet2;
        this.bet1 = false;
        this.bet3 = false;
    }

    toggleBet3() {
        this.bet3 = !this.bet3;
        this.bet2 = false;
        this.bet1 = false;
    }
}
