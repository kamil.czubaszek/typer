import { Route } from '@angular/router';
import {RulesComponent} from './rules.component';

export const RULES_ROUTE: Route = {
    path: 'rules',
    component: RulesComponent,
    data: {
        authorities: [],
        pageTitle: 'rules.title'
    }
};
