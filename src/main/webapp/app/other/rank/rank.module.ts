import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import {TyperSharedModule} from '../../shared';
import {RankComponent} from './rank.component';
import {rankRoute} from './rank.route';
import {LeagueService} from '../../entities/league';
import {RankDetailComponent} from './rank-detail.component';

const STATES = [
    ...rankRoute,
];

@NgModule({
    imports: [
        TyperSharedModule,
        RouterModule.forChild(STATES)
    ],
    declarations: [
        RankComponent,
        RankDetailComponent
    ],
    entryComponents: [
    ],
    providers: [
        LeagueService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TyperRankModule {}
