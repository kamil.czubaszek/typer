import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';
import {LeagueService} from '../../entities/league/league.service';

@Component({
    selector: 'jhi-rank-detail',
    templateUrl: './rank-detail.component.html'
})
export class RankDetailComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    public rank: Map<string, number>;
    private rankId: number;

    constructor(
        private eventManager: JhiEventManager,
        private leagueService: LeagueService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.rankId = params['id'];
            this.load(params['id']);
        });
        this.registerChangeInLeagues();
    }

    load(id) {
        this.leagueService.getRank(id).subscribe
        ((leagueResponse: HttpResponse<Map<string, number>>) => {
            this.rank = leagueResponse.body;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInLeagues() {
        this.eventSubscriber = this.eventManager.subscribe(
            'rankModification',
            (response) => this.load(this.rankId)
        );
    }

    getKeys() {
        return Array.from(this.rank.keys());
    }
}
