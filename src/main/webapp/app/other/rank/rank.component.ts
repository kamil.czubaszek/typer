import {Component, OnInit} from '@angular/core';
import {League, LeagueComponent, LeagueService} from '../../entities/league';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService} from 'ng-jhipster';

@Component({
    selector: 'jhi-rank',
    templateUrl: './rank.component.html'

})
export class RankComponent implements OnInit {
    leagues: League[];
    pointsLoaded: Promise<boolean>;

    constructor(
        private leagueService: LeagueService,
        private jhiAlertService: JhiAlertService
        ) {}

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {
        this.leagueService.query().subscribe(
            (res: HttpResponse<League[]>) => {
                this.leagues = res.body;
                this.additonalInit(this.leagues);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    additonalInit(l) {
        for (const league of l){
            this.loadPoints(league);
        }
    }

    loadPoints(league) {
        this.leagueService.getPoints(league.id)
            .subscribe((leagueResponse: HttpResponse<number>) => {
                league.userPoints = leagueResponse.body;
                this.pointsLoaded = Promise.resolve(true);
            });
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
