import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import {RankComponent} from './rank.component';
import {RankDetailComponent} from './rank-detail.component';

export const rankRoute: Routes = [
    {
        path: 'rank',
        component: RankComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'typerApp.rank.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'rank/:id',
        component: RankDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'typerApp.rank.detail.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
