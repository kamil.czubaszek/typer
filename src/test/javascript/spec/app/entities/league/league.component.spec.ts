/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TyperTestModule } from '../../../test.module';
import { LeagueComponent } from '../../../../../../main/webapp/app/entities/league/league.component';
import { LeagueService } from '../../../../../../main/webapp/app/entities/league/league.service';
import { League } from '../../../../../../main/webapp/app/entities/league/league.model';

describe('Component Tests', () => {

    describe('League Management Component', () => {
        let comp: LeagueComponent;
        let fixture: ComponentFixture<LeagueComponent>;
        let service: LeagueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TyperTestModule],
                declarations: [LeagueComponent],
                providers: [
                    LeagueService
                ]
            })
            .overrideTemplate(LeagueComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LeagueComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LeagueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new League(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.leagues[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
