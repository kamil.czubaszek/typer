/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TyperTestModule } from '../../../test.module';
import { LeagueDetailComponent } from '../../../../../../main/webapp/app/entities/league/league-detail.component';
import { LeagueService } from '../../../../../../main/webapp/app/entities/league/league.service';
import { League } from '../../../../../../main/webapp/app/entities/league/league.model';

describe('Component Tests', () => {

    describe('League Management Detail Component', () => {
        let comp: LeagueDetailComponent;
        let fixture: ComponentFixture<LeagueDetailComponent>;
        let service: LeagueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TyperTestModule],
                declarations: [LeagueDetailComponent],
                providers: [
                    LeagueService
                ]
            })
            .overrideTemplate(LeagueDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(LeagueDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(LeagueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new League(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.league).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
