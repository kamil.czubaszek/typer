import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Bet e2e test', () => {

    let navBarPage: NavBarPage;
    let betDialogPage: BetDialogPage;
    let betComponentsPage: BetComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Bets', () => {
        navBarPage.goToEntity('bet');
        betComponentsPage = new BetComponentsPage();
        expect(betComponentsPage.getTitle())
            .toMatch(/typerApp.bet.home.title/);

    });

    it('should load create Bet dialog', () => {
        betComponentsPage.clickOnCreateButton();
        betDialogPage = new BetDialogPage();
        expect(betDialogPage.getModalTitle())
            .toMatch(/typerApp.bet.home.createOrEditLabel/);
        betDialogPage.close();
    });

   /* it('should create and save Bets', () => {
        betComponentsPage.clickOnCreateButton();
        betDialogPage.setDateInput(12310020012301);
        expect(betDialogPage.getDateInput()).toMatch('2001-12-31T02:30');
        betDialogPage.setHostResultInput('5');
        expect(betDialogPage.getHostResultInput()).toMatch('5');
        betDialogPage.setGuestResultInput('5');
        expect(betDialogPage.getGuestResultInput()).toMatch('5');
        betDialogPage.setTypeInput('5');
        expect(betDialogPage.getTypeInput()).toMatch('5');
        betDialogPage.setWinnerInput('5');
        expect(betDialogPage.getWinnerInput()).toMatch('5');
        betDialogPage.gameSelectLastOption();
        betDialogPage.userSelectLastOption();
        betDialogPage.save();
        expect(betDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });*/

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class BetComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-bet div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BetDialogPage {
    modalTitle = element(by.css('h4#myBetLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateInput = element(by.css('input#field_date'));
    hostResultInput = element(by.css('input#field_hostResult'));
    guestResultInput = element(by.css('input#field_guestResult'));
    typeInput = element(by.css('input#field_type'));
    winnerInput = element(by.css('input#field_winner'));
    gameSelect = element(by.css('select#field_game'));
    userSelect = element(by.css('select#field_user'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDateInput = function(date) {
        this.dateInput.sendKeys(date);
    };

    getDateInput = function() {
        return this.dateInput.getAttribute('value');
    };

    setHostResultInput = function(hostResult) {
        this.hostResultInput.sendKeys(hostResult);
    };

    getHostResultInput = function() {
        return this.hostResultInput.getAttribute('value');
    };

    setGuestResultInput = function(guestResult) {
        this.guestResultInput.sendKeys(guestResult);
    };

    getGuestResultInput = function() {
        return this.guestResultInput.getAttribute('value');
    };

    setTypeInput = function(type) {
        this.typeInput.sendKeys(type);
    };

    getTypeInput = function() {
        return this.typeInput.getAttribute('value');
    };

    setWinnerInput = function(winner) {
        this.winnerInput.sendKeys(winner);
    };

    getWinnerInput = function() {
        return this.winnerInput.getAttribute('value');
    };

    gameSelectLastOption = function() {
        this.gameSelect.all(by.tagName('option')).last().click();
    };

    gameSelectOption = function(option) {
        this.gameSelect.sendKeys(option);
    };

    getGameSelect = function() {
        return this.gameSelect;
    };

    getGameSelectedOption = function() {
        return this.gameSelect.element(by.css('option:checked')).getText();
    };

    userSelectLastOption = function() {
        this.userSelect.all(by.tagName('option')).last().click();
    };

    userSelectOption = function(option) {
        this.userSelect.sendKeys(option);
    };

    getUserSelect = function() {
        return this.userSelect;
    };

    getUserSelectedOption = function() {
        return this.userSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
