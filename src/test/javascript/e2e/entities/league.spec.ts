import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('League e2e test', () => {

    let navBarPage: NavBarPage;
    let leagueDialogPage: LeagueDialogPage;
    let leagueComponentsPage: LeagueComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Leagues', () => {
        navBarPage.goToEntity('league');
        leagueComponentsPage = new LeagueComponentsPage();
        expect(leagueComponentsPage.getTitle())
            .toMatch(/typerApp.league.home.title/);

    });

    it('should load create League dialog', () => {
        leagueComponentsPage.clickOnCreateButton();
        leagueDialogPage = new LeagueDialogPage();
        expect(leagueDialogPage.getModalTitle())
            .toMatch(/typerApp.league.home.createOrEditLabel/);
        leagueDialogPage.close();
    });

    it('should create and save Leagues', () => {
        leagueComponentsPage.clickOnCreateButton();
        leagueDialogPage.setNameInput('name');
        expect(leagueDialogPage.getNameInput()).toMatch('name');
        // leagueDialogPage.teamSelectLastOption();
        leagueDialogPage.save();
        expect(leagueDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class LeagueComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-league div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class LeagueDialogPage {
    modalTitle = element(by.css('h4#myLeagueLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    teamSelect = element(by.css('select#field_team'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    teamSelectLastOption = function() {
        this.teamSelect.all(by.tagName('option')).last().click();
    };

    teamSelectOption = function(option) {
        this.teamSelect.sendKeys(option);
    };

    getTeamSelect = function() {
        return this.teamSelect;
    };

    getTeamSelectedOption = function() {
        return this.teamSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
