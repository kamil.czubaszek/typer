import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Game e2e test', () => {

    let navBarPage: NavBarPage;
    let gameDialogPage: GameDialogPage;
    let gameComponentsPage: GameComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Games', () => {
        navBarPage.goToEntity('game');
        gameComponentsPage = new GameComponentsPage();
        expect(gameComponentsPage.getTitle())
            .toMatch(/typerApp.game.home.title/);

    });

    it('should load create Game dialog', () => {
        gameComponentsPage.clickOnCreateButton();
        gameDialogPage = new GameDialogPage();
        expect(gameDialogPage.getModalTitle())
            .toMatch(/typerApp.game.home.createOrEditLabel/);
        gameDialogPage.close();
    });

   /* it('should create and save Games', () => {
        gameComponentsPage.clickOnCreateButton();
        gameDialogPage.setDateInput(12310020012301);
        expect(gameDialogPage.getDateInput()).toMatch('2001-12-31T02:30');
        gameDialogPage.setHostResultInput('5');
        expect(gameDialogPage.getHostResultInput()).toMatch('5');
        gameDialogPage.setGuestResultInput('5');
        expect(gameDialogPage.getGuestResultInput()).toMatch('5');
        gameDialogPage.hostSelectLastOption();
        gameDialogPage.guestSelectLastOption();
        gameDialogPage.leagueSelectLastOption();
        gameDialogPage.save();
        expect(gameDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });*/

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class GameComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-game div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class GameDialogPage {
    modalTitle = element(by.css('h4#myGameLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateInput = element(by.css('input#field_date'));
    hostResultInput = element(by.css('input#field_hostResult'));
    guestResultInput = element(by.css('input#field_guestResult'));
    hostSelect = element(by.css('select#field_host'));
    guestSelect = element(by.css('select#field_guest'));
    leagueSelect = element(by.css('select#field_league'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDateInput = function(date) {
        this.dateInput.sendKeys(date);
    };

    getDateInput = function() {
        return this.dateInput.getAttribute('value');
    };

    setHostResultInput = function(hostResult) {
        this.hostResultInput.sendKeys(hostResult);
    };

    getHostResultInput = function() {
        return this.hostResultInput.getAttribute('value');
    };

    setGuestResultInput = function(guestResult) {
        this.guestResultInput.sendKeys(guestResult);
    };

    getGuestResultInput = function() {
        return this.guestResultInput.getAttribute('value');
    };

    hostSelectLastOption = function() {
        this.hostSelect.all(by.tagName('option')).last().click();
    };

    hostSelectOption = function(option) {
        this.hostSelect.sendKeys(option);
    };

    getHostSelect = function() {
        return this.hostSelect;
    };

    getHostSelectedOption = function() {
        return this.hostSelect.element(by.css('option:checked')).getText();
    };

    guestSelectLastOption = function() {
        this.guestSelect.all(by.tagName('option')).last().click();
    };

    guestSelectOption = function(option) {
        this.guestSelect.sendKeys(option);
    };

    getGuestSelect = function() {
        return this.guestSelect;
    };

    getGuestSelectedOption = function() {
        return this.guestSelect.element(by.css('option:checked')).getText();
    };

    leagueSelectLastOption = function() {
        this.leagueSelect.all(by.tagName('option')).last().click();
    };

    leagueSelectOption = function(option) {
        this.leagueSelect.sendKeys(option);
    };

    getLeagueSelect = function() {
        return this.leagueSelect;
    };

    getLeagueSelectedOption = function() {
        return this.leagueSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
