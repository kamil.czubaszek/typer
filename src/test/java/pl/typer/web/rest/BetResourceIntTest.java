package pl.typer.web.rest;

import pl.typer.TyperApp;

import pl.typer.domain.Bet;
import pl.typer.domain.Game;
import pl.typer.repository.BetRepository;
import pl.typer.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pl.typer.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BetResource REST controller.
 *
 * @see BetResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TyperApp.class)
public class BetResourceIntTest {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_HOST_RESULT = 0;
    private static final Integer UPDATED_HOST_RESULT = 1;

    private static final Integer DEFAULT_GUEST_RESULT = 0;
    private static final Integer UPDATED_GUEST_RESULT = 1;

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    private static final Integer DEFAULT_WINNER = 1;
    private static final Integer UPDATED_WINNER = 2;

    @Autowired
    private BetRepository betRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBetMockMvc;

    private Bet bet;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BetResource betResource = new BetResource(betRepository);
        this.restBetMockMvc = MockMvcBuilders.standaloneSetup(betResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bet createEntity(EntityManager em) {
        Bet bet = new Bet()
            .date(DEFAULT_DATE)
            .hostResult(DEFAULT_HOST_RESULT)
            .guestResult(DEFAULT_GUEST_RESULT)
            .type(DEFAULT_TYPE)
            .winner(DEFAULT_WINNER);
        // Add required entity
        Game game = GameResourceIntTest.createEntity(em);
        em.persist(game);
        em.flush();
        bet.setGame(game);
        return bet;
    }

    @Before
    public void initTest() {
        bet = createEntity(em);
    }

    @Test
    @Transactional
    public void createBet() throws Exception {
        int databaseSizeBeforeCreate = betRepository.findAll().size();

        // Create the Bet
        restBetMockMvc.perform(post("/api/bets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bet)))
            .andExpect(status().isCreated());

        // Validate the Bet in the database
        List<Bet> betList = betRepository.findAll();
        assertThat(betList).hasSize(databaseSizeBeforeCreate + 1);
        Bet testBet = betList.get(betList.size() - 1);
        assertThat(testBet.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testBet.getHostResult()).isEqualTo(DEFAULT_HOST_RESULT);
        assertThat(testBet.getGuestResult()).isEqualTo(DEFAULT_GUEST_RESULT);
        assertThat(testBet.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testBet.getWinner()).isEqualTo(DEFAULT_WINNER);
    }

    @Test
    @Transactional
    public void createBetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = betRepository.findAll().size();

        // Create the Bet with an existing ID
        bet.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBetMockMvc.perform(post("/api/bets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bet)))
            .andExpect(status().isBadRequest());

        // Validate the Bet in the database
        List<Bet> betList = betRepository.findAll();
        assertThat(betList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkHostResultIsRequired() throws Exception {
        int databaseSizeBeforeTest = betRepository.findAll().size();
        // set the field null
        bet.setHostResult(null);

        // Create the Bet, which fails.

        restBetMockMvc.perform(post("/api/bets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bet)))
            .andExpect(status().isBadRequest());

        List<Bet> betList = betRepository.findAll();
        assertThat(betList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGuestResultIsRequired() throws Exception {
        int databaseSizeBeforeTest = betRepository.findAll().size();
        // set the field null
        bet.setGuestResult(null);

        // Create the Bet, which fails.

        restBetMockMvc.perform(post("/api/bets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bet)))
            .andExpect(status().isBadRequest());

        List<Bet> betList = betRepository.findAll();
        assertThat(betList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBets() throws Exception {
        // Initialize the database
        betRepository.saveAndFlush(bet);

        // Get all the betList
        restBetMockMvc.perform(get("/api/bets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bet.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].hostResult").value(hasItem(DEFAULT_HOST_RESULT)))
            .andExpect(jsonPath("$.[*].guestResult").value(hasItem(DEFAULT_GUEST_RESULT)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].winner").value(hasItem(DEFAULT_WINNER)));
    }

    @Test
    @Transactional
    public void getBet() throws Exception {
        // Initialize the database
        betRepository.saveAndFlush(bet);

        // Get the bet
        restBetMockMvc.perform(get("/api/bets/{id}", bet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bet.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.hostResult").value(DEFAULT_HOST_RESULT))
            .andExpect(jsonPath("$.guestResult").value(DEFAULT_GUEST_RESULT))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.winner").value(DEFAULT_WINNER));
    }

    @Test
    @Transactional
    public void getNonExistingBet() throws Exception {
        // Get the bet
        restBetMockMvc.perform(get("/api/bets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBet() throws Exception {
        // Initialize the database
        betRepository.saveAndFlush(bet);
        int databaseSizeBeforeUpdate = betRepository.findAll().size();

        // Update the bet
        Bet updatedBet = betRepository.findOne(bet.getId());
        // Disconnect from session so that the updates on updatedBet are not directly saved in db
        em.detach(updatedBet);
        updatedBet
            .date(UPDATED_DATE)
            .hostResult(UPDATED_HOST_RESULT)
            .guestResult(UPDATED_GUEST_RESULT)
            .type(UPDATED_TYPE)
            .winner(UPDATED_WINNER);

        restBetMockMvc.perform(put("/api/bets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBet)))
            .andExpect(status().isOk());

        // Validate the Bet in the database
        List<Bet> betList = betRepository.findAll();
        assertThat(betList).hasSize(databaseSizeBeforeUpdate);
        Bet testBet = betList.get(betList.size() - 1);
        assertThat(testBet.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testBet.getHostResult()).isEqualTo(UPDATED_HOST_RESULT);
        assertThat(testBet.getGuestResult()).isEqualTo(UPDATED_GUEST_RESULT);
        assertThat(testBet.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testBet.getWinner()).isEqualTo(UPDATED_WINNER);
    }

    @Test
    @Transactional
    public void updateNonExistingBet() throws Exception {
        int databaseSizeBeforeUpdate = betRepository.findAll().size();

        // Create the Bet

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBetMockMvc.perform(put("/api/bets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bet)))
            .andExpect(status().isCreated());

        // Validate the Bet in the database
        List<Bet> betList = betRepository.findAll();
        assertThat(betList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBet() throws Exception {
        // Initialize the database
        betRepository.saveAndFlush(bet);
        int databaseSizeBeforeDelete = betRepository.findAll().size();

        // Get the bet
        restBetMockMvc.perform(delete("/api/bets/{id}", bet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Bet> betList = betRepository.findAll();
        assertThat(betList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Bet.class);
        Bet bet1 = new Bet();
        bet1.setId(1L);
        Bet bet2 = new Bet();
        bet2.setId(bet1.getId());
        assertThat(bet1).isEqualTo(bet2);
        bet2.setId(2L);
        assertThat(bet1).isNotEqualTo(bet2);
        bet1.setId(null);
        assertThat(bet1).isNotEqualTo(bet2);
    }
}
